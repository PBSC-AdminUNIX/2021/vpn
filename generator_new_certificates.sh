#!/bin/bash

file=$1

EASY_RSA_DIR=$HOME/easy-rsa
CLIENT_FILES_DIR=$HOME/client-configs/keys/*
DIR_ALL_FILES_TAR_GZ=$HOME/all_files_clients/
client_key=client.key
client_cert=client.crt

text_main() {
    echo "Script para generar usuarios con EASY-RSA"
    echo -e "\nQuiere generar certificados con password [S/n]?"
    read -p ">> " input_password_or_nopassword
}

error_general() {
    echo "Opcion no valida"
    exit 1
}

set_variables_certificate() {
    CLIENT_KEY_DIR=$HOME/easy-rsa/pki/private/$name_certificatfile.key
    CLIENT_CERT_DIR=$HOME/easy-rsa/pki/issued/$name_certificatfile.crt
    DIR_ALL_FILES_CLIENT=$HOME/client-configs/files/$name_certificatfile/
    CLIENT_REQ_DIR=$HOME/easy-rsa/pki/reqs/$name_certificatfile.req
}

create_tar_gz_client() {
    cd $DIR_ALL_FILES_CLIENT
    sudo chown root:root *
    sudo tar -cvvpf $name_certificatfile.tar.gz ./*
    sudo chown $USER:users $name_certificatfile.tar.gz
    mv $name_certificatfile.tar.gz $DIR_ALL_FILES_TAR_GZ
    sudo rm -rf $DIR_ALL_FILES_CLIENT
}

create_certificate() {
    cd $EASY_RSA_DIR
    if [ $certificate_with_password -eq 1 ]; then
        echo -en "\n" | ./easyrsa gen-req $name_certificatfile
    else
        echo -en "\n" | ./easyrsa gen-req $name_certificatfile nopass
    fi
    echo "yes" | ./easyrsa sign-req client $name_certificatfile
    mkdir $DIR_ALL_FILES_CLIENT
    chmod 0700 $DIR_ALL_FILES_CLIENT
    mv $CLIENT_KEY_DIR $DIR_ALL_FILES_CLIENT$client_key
    mv $CLIENT_CERT_DIR $DIR_ALL_FILES_CLIENT$client_cert
    cp $CLIENT_FILES_DIR $DIR_ALL_FILES_CLIENT
    rm $CLIENT_REQ_DIR
}

generate_certificates() {
    line_number=0
    while IFS= read -r line; do
        line_number=$(($line_number + 1))
        name_certificatfile=$line
        set_variables_certificate
        create_certificate
        create_tar_gz_client
    done <"$file"
}

main() {
    text_main
    if [ -n "$input_password_or_nopassword" ]; then
        if [ "$input_password_or_nopassword" = "s" ] || [ "$input_password_or_nopassword" = "S" ]; then
            certificate_with_password=1
        elif [ "$input_password_or_nopassword" = "n" ] || [ "$input_password_or_nopassword" = "N" ]; then
            certificate_with_password=0
        else
            error_general
        fi
        generate_certificates
    else
        error_general
    fi
}

main
