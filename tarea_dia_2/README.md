# Script para crear usuarios en bash
## Funcionamiento y requerimientos
* Para el correcto funcionamiento del script se necesita correr con permisos de administrador
* sudo ./newusers.sh ruta_del_archivo - esta seria la forma correcta de ejecutar el script
* El programa utiliza un archivo donde previamente hay usuarios con datos adiciones con el siguiente formato
** usuario:contraseña:uid:gid:gecos:home:shell 
** Cada usuario que se desea agregar se hara en una linea diferente
* Al correr el script se mostraran errores si es que los hay para que el usuario sepa donde esta el error
* Tambien se mostrara la informacion insertada en el sistema si no hubo errores, la informacion se recopila del archivo ingresado por el usuario
# Este script fue probado correctamente en GNU/Linux Debian 11